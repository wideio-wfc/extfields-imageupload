#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
"""
WIDE IO
Revision FILE: $Id: widgets.py 841 2009-08-23 13:01:52Z tranx $


#############################################################################################################
  copyright $Copyright$
  @version $Revision: 841 $
  @lastrevision $Date: 2009-08-23 22:01:52 +0900 (Sun, 23 Aug 2009) $
  @modifiedby $LastChangedBy: tranx $
  @lastmodified $LastChangedDate: 2009-08-23 22:01:52 +0900 (Sun, 23 Aug 2009) $
#############################################################################################################
"""
# create widgets related to jquery component for essential displays ...
import datetime
import re
import uuid

from django.db.models.fields import NOT_PROVIDED
from django.forms.widgets import Widget
from django.template import Context, loader
from django.utils.safestring import mark_safe


from extfields.imageupload import fields

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

import  settings


def getuuid():
    return str(uuid.uuid1())

class AImageWidget(Widget):
    class Media:
        css = {'all': [settings.MEDIA_URL + "/css/extfields-imageupload.css"]}
        js = [
            "https://www.google.com/jsapi",
            settings.MEDIA_URL +
            "/js/_extfields-imageupload.dbg.js"]

    """
    For uploading AJAX-ready django images...
    """

    def __init__(self, attrs={}):
        self.attrs = attrs
        self.uuid = getuuid()
        self.request = None

    def set_request(self, r):
        self.request = r

    def render(self, name, ovalue, attrs=None):
        from references.models import Image
        # if (value==None):
        # < create a temporary image object on which we work..
        # < if the image is modified or accepted we will change the value
        # < other wise we should return the previous reference image
        if type(ovalue) in [unicode, str]:
            try:
              ovalue = Image.objects.get(id=ovalue)
            except Exception,o:
              print o, ovalue
              ovalue=None
        
        print "My ovalue = {0}".format(repr(ovalue))
        if ovalue is None:
          value = Image()
          value.set_expire(
          expire=datetime.timedelta(minutes=10)) 
          value.reference = ovalue
          value.wiostate='U0'
          value.save()
        #if ovalue is not None:
            #value.image = ovalue.image  # TODO: CHECK IF THIS FAST OR SLOW
        #else:
            #value.image = None
          value.on_add(self.request)
          value.save()
        else:
          value=ovalue
        widgettemplate = loader.get_template('extfields/aimage.html')
        html_content = widgettemplate.render(
            Context(
                {
                    'name': name,
                    'value': value,
                    'MEDIA_URL': settings.MEDIA_URL,
                    'uuid': self.uuid,
                    'public_images': Image.objects.filter(
                        is_public=True)| Image.objects.filter(
                          owner=self.request.user,wiostate='V')}))
        return html_content

    def get_prologue(self):
        # multipart/form-data
        return """
        <div style="visibility:hidden;height:1px;width:1px;">
            <form id="aimage-form-%(uuid)s" action="%(update_url)s" method="POST" enctype="multipart/form-data">
                <input type="file" name="image" />
                <input type="hidden" name="name" value="truc" />
                <input type="hidden" name="optional_source_url" value="http://www.google.com/"/>
                <input type="submit" value="Upload Image" />
            </form>
        </div>
        """ % ({"update_url": "not-yet-set", "uuid": self.uuid})

    def value_from_datadict(self, data, files, name):
        import urllib
        from references.models import Image
        from django.core.files.base import ContentFile
        if name not in data:
            print "DATA DOESN'T HAVE KEY", name
            print data
            return None

        value = data[name]
        if value==None or (hasattr(value,"__len__") and  len(value)==0):
          return None
        if hasattr(value, "__getitem__") and value[0] == '#':
            # then we know it's a reference and need to download it
            url = value[1:]
        else:
            return value

        # TODO : WE MAY NEED TO REMOVE THE PREVIOUS IMAGE (UNLESS ITS DONE
        # AUTOMATICALLY BY THE DB ENGINE ? - BUT EVEN SO EH NEED TO BE CALLED)
        previ = Image.objects.filter(orig_url=url)

        if previ.count():
            return previ[0].id
        ni = Image()
        # ni.set_expire(expire=datetime.timedelta(minutes=10)) # make it expire
        ni.orig_url = url
        ni.wiostate='V'
        ni.save()

        from urlparse import urlparse
        from os.path import splitext, basename
        disassembled = urlparse(url)
        filename, file_ext = splitext(basename(disassembled.path))

        new_content = StringIO()
        new_content.write(urllib.urlopen(url).read())
        new_content = ContentFile(new_content.getvalue())
        # ni.image.save("img-"+str(ni.id),new_content) # FIXME: TODO: OUTSIDE
        # OF SERVER THREADS
        image_name = filename + file_ext
        #ni.image.save(image_name, new_content)
        fields.PyrImageField.pyr_save(ni.image,image_name,new_content)
        ni.on_add(self.request)        
        ni.save()
        data[name]=ni.id
        print "saved image url", url, ni.id
        return ni.id
