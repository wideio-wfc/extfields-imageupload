# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

import os
import uuid
import hashlib
import sys

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

from django.db.models import ForeignKey
from django.db.models.fields.files import ImageField, ImageFieldFile
from django.core.files.base import ContentFile
from django.conf import settings


def _update_ext(filename, new_ext):
    parts = filename.split('.')
    parts[-1] = new_ext
    return '.'.join(parts)


class ResizedImageField(ImageField):
    #attr_class = ResizedImageFieldFile

    def save(self, name, content, save=True):
        from PIL import Image
        try:
            new_content = StringIO()
            content.file.seek(0)

            # print content.file
            img = Image.open(content.file)
            img=img.thumbnail((
                self.field.max_width,
                self.field.max_height
            ), Image.ANTIALIAS)
            img.save(new_content, format=self.field.format)
            new_content = ContentFile(new_content.getvalue())
            new_name = _update_ext(name, self.field.format.lower())
            super(ResizedImageField, self).save(new_name, new_content, save)
        except Exception as e:
            sys.stderr.write(
                "[imageupload/fields] Error in reading image " +
                str(e) +
                "\n")
            content = None

    def __init__(
            self, max_width=100, max_height=100, format='PNG', *args, **kwargs):
        self.max_width = max_width
        self.max_height = max_height
        self.format = format
        super(ResizedImageField, self).__init__(*args, **kwargs)


def qlog2(i):
  j=0
  while (i>0):
    i=i>>1
    j+=1
  r=1
  for c in range(j-1):
    r=r<<1
  return r

class PyrImageField(ImageField):
    @staticmethod
    def pyr_get(self,cw=128,ch=128):
      new_name= self.name      
      if new_name==None:
        return ""
      if os.path.exists(new_name):
        return new_name
      bsz=qlog2(max(max(self.field.min_width,cw),max(self.field.min_height,ch)))
      while (bsz>1):
         tname="%s/%s/%s/%s-%s.%s"%(self.field.upload_to,new_name[:3],new_name[3:6],new_name[6:], str(bsz), self.field.format.lower() )
         if os.path.exists(tname):
            return tname
         bsz/=2
      bsz=qlog2(max(max(self.field.min_width,cw),max(self.field.min_height,ch)))
      while (bsz<4096):
         tname="%s/%s/%s/%s-%s.%s"%(self.field.upload_to,new_name[:3],new_name[3:6],new_name[6:], str(bsz), self.field.format.lower() )
         if os.path.exists(tname):
            return tname
         bsz*=2
      if settings.PRODUCTION_MODE:
          return None
      else:
          return new_name+"!"+self.name+"!"

    @staticmethod
    def pyr_delete(self):
            cw=self.field.max_width
            ch=self.field.max_height
            new_name = self.name
            while (cw>=self.field.min_width) and (ch>=self.field.min_height):
              img.thumbnail((
                 self.field.max_width,
                 self.field.max_height
              ), Image.ANTIALIAS)              
              tname="%s/%s/%s/%s-%s.%s"%(self.field.upload_to,new_name[:3],new_name[3:6],new_name[6:], str(qlog2(max(cw,ch))), self.field.format.lower() )
              if os.path.exists(tname):
                os.unlink(tname)
    
    @staticmethod
    def pyr_save(self, name, content, save=True):
        from PIL import Image        
        try:
            #new_content = StringIO()
            content.file.seek(0)

            # print content.file
            img = Image.open(content.file)#            
            cw=min(self.field.max_width,img.size[0])
            ch=min(self.field.max_height,img.size[1])
            new_name = hashlib.md5(str(uuid.uuid1())).hexdigest()
            while (cw>=self.field.min_width) or (ch>=self.field.min_height):
              img.thumbnail((
                 cw,
                 ch
              ), Image.ANTIALIAS)
              
              tname="%s/%s/%s/%s-%s.%s"%(self.field.upload_to,new_name[:3],new_name[3:6],new_name[6:], str(qlog2(max(cw,ch))), self.field.format.lower() )
              if self.field.upload_to[0]=='/':
                r=['']
              else:
                r=[]
              for c in tname.split('/')[:-1]:
                r.append(c)
                cd='/'.join(r)
                if not os.path.exists(cd):
                  os.mkdir(cd)
              print tname
              img.save(tname, format=self.field.format)
              cw/=2
              ch/=2
             
            new_content=None
             
            #new_content = ContentFile(new_content.getvalue())            
            self.name = new_name
            
            #super(ResizedImageField, self).save(new_name, new_content, save)
        except Exception as e:
            sys.stderr.write(
                "[imageupload/fields] Error in reading image " +
                str(e) +
                "\n")
            content = None
            raise

    def __init__(
            self,  max_width=512, max_height=512, min_width=32, min_height=32,  format='PNG', *args, **kwargs):        
        self.max_width = max_width
        self.max_height = max_height
        self.min_width = min_width
        self.min_height = min_height

        self.format = format
        super(PyrImageField, self).__init__(*args, **kwargs)


def RefImageField(*args, **kwargs):
    return ForeignKey('references.Image')
